# 0.1.0 / 2023-11-03

  * BREAKING CHANGE: portrait video support and preserve aspect ratio,
    use `--scale=1280x720` to restore previous behavior
  * add option to disable timecode, `--no-timecode`
  * fix crash in newer ffpmeg version

# 0.0.1 / 2022-06-16

 * first public release
